/*=== IMPORT STYLES */
// import '@progress/kendo-theme-material/dist/all.css';
import '@progress/kendo-theme-default/dist/all.css';
import * as React from 'react';
import * as ReactDOM from 'react-dom';

import registerServiceWorker from './registerServiceWorker';
import {Provider} from "mobx-react";
import {AppStore} from "./AppStore";
import {App} from "./App";
// import DevTools from 'mobx-react-devtools';
/*=== END IMPORT STYLES */


const store = {
  appStore: new AppStore()
};

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>

  , document.getElementById('root'));


registerServiceWorker();
