import * as  React from 'react';
import {Redirect, Route, Switch} from "react-router";
import {BrowserRouter} from "react-router-dom";
import './app.css';
import {inject, observer} from "mobx-react";
import Login from "./views/Login";
import Dashboard from "./views/Dashboard";
import {AuthenticatedRoute} from "./components/AuthenticatedRoute";
import {AppSnackbar} from "./components/snackbars/AppSnackbar";


@inject('store')
@observer
export class App extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
  }
  render() {
    return(
      <BrowserRouter>
        <div>
          <AppSnackbar />
          <Switch>
            <AuthenticatedRoute path='/dashboard' component={Dashboard}/>
            <Route path='/login' component={Login}/>
            <Route render={() => <Redirect to={"/dashboard/calendar"}/>}/>
          </Switch>
        </div>
      </BrowserRouter>
    )
  }
}
