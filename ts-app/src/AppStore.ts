import {action, observable} from 'mobx';
import api from "./api";
import {AxiosResponse} from "axios";
import {FYSIOSPORT_TOKEN, FYSIOSPORT_USER, FYSIOSPSORT_ROLE_SUPER_ADMIN} from "./constants";
import {AppSnackbar} from "./components/snackbars/AppSnackbar";

export class AppStore {
  @observable appSnackbar: AppSnackbar;
  @observable isAuthenticated: boolean;
  @observable authenticatedUser: any;
  @observable events: any = [];
  @observable schedules: any = [];
  @observable selectedEvent: any = {
    id: '',
    dateString: {
      en: '', nl: ''
    },
    attendees: [],
    blockhour: {
      startTime: '',
      endTime: '',
      tag: ''
    }
  };

  constructor() {
  }

  selectEvent(event: any) {
    this.selectedEvent = event;
  }

  setAuthenticated(value: boolean) {
    this.isAuthenticated = value;
  }

  @action getAuthenticated() {
    return this.isAuthenticated;
  }

  @action isSuperAdmin(): boolean {
    try {
      const user = JSON.parse(localStorage.getItem(FYSIOSPORT_USER) as any);
      if (localStorage.getItem(FYSIOSPORT_USER) && user && (user.role === FYSIOSPSORT_ROLE_SUPER_ADMIN)) {
        return true;
      }
    } catch (e) {
      return false;
    }
    return false;
  }

  @action
  async fetchAllEvents() {
    try {
      const response: AxiosResponse = await api.get(`/event`);
      this.events = response.data;
    } catch (e) {

    }
  }

  @action
  async fetchAllSchedules() {
    try {
      const response: AxiosResponse = await api.get(`/schedule`);
      this.schedules = response.data;
    } catch (e) {

    }
  }


  @action
  async verifyToken() {
    try {
      const response = await api.post(`verify-token`, {
        token: localStorage.getItem(FYSIOSPORT_TOKEN)
      });
      this.setAuthenticated(true);
      this.authenticatedUser = response.data;
    } catch (e) {
      this.setAuthenticated(false);
    }
  }
}
