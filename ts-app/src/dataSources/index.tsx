import '@progress/kendo-ui';
const userDataSource = new kendo.data.DataSource({
  transport: {
    read: {
      url: "/api/datasource/user",
    }
  }
});

export {userDataSource};

