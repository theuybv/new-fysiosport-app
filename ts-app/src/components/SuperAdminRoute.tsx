import * as React from 'react';
import {inject, observer} from "mobx-react";
import {Redirect, Route} from "react-router";
import {FYSIOSPSORT_ROLE_SUPER_ADMIN} from "../constants";

interface IState {
  isVerifying: boolean
}

@inject('store')
@observer
export class SuperAdminRoute extends React.Component<any, IState> {

  constructor(props: any) {
    super(props);

  }
  render() {
    return (
      <Route render={() => {
        return (
          (this.props.store.appStore.authenticatedUser.role === FYSIOSPSORT_ROLE_SUPER_ADMIN)
            ? <this.props.component/>
            : <Redirect to="/"/>
        )
      }}/>
    )
  }

}
