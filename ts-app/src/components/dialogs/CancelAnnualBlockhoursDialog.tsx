import AlertDialog from "./AlertDialog";
import * as React from "react";

export const CancelAnnualBlockhoursDialog = (props: { open: boolean, positiveAction: Function, negativeAction: Function }) => {
  return (
    <AlertDialog open={props.open}
                 title={`Weet u het zeker?`}
                 positiveAction={props.positiveAction}
                 positiveButtonText={`Ok, verwijder uit de planning`}
                 message={`U staat op het punt de geselecteerde FysioSporters uit de planning te halen.`}
                 negativeAction={props.negativeAction}
                 negativeButtonText={`Sluiten`}/>
  );
};
