import * as React from 'react';
import {Dialog} from '@progress/kendo-dialog-react-wrapper';

import DialogEvent = kendo.ui.DialogEvent;
import DialogAction = kendo.ui.DialogAction;
import MultiSelect from "@progress/kendo-dropdowns-react-wrapper/dist/npm/multiselect";
import {UserMultiSelect} from "../UserMultiSelect";

interface IEventDialog {
  event: any;
  save: any;
  forwardRef: any;
}

function Component() {
  class EventDialog extends React.Component<IEventDialog, any> {
    dialogRef: any = React.createRef<Dialog>();
    cancelAction: DialogAction = {
      text: "Annuleren",
      action: (e: DialogEvent) => {
        e.sender.close()
      }
    };
    positiveAction: DialogAction = {
      text: "Opslaan",
      primary: true,
      action: this.positiveButtonActionHandler.bind(this)
    };

    actions = [this.cancelAction, this.positiveAction];
    multiSelectRef: any = React.createRef<MultiSelect>();


    constructor(props: any) {
      super(props);
    }

    render() {
      const {event, forwardRef} = this.props;

      return (
        <div>
          <Dialog ref={forwardRef}
                  visible={false}
                  minWidth={800}
                  width={800}
                  actions={this.actions}
          >
            <div className="container-fluid">
              <div className="row">
                <div className="col-md-12">
                  <div className="card">
                    <div className="card-body">
                      <form className="k-form-inline">
                        <fieldset>
                          <legend><i className="k-icon k-i-calendar"></i> &nbsp; {event.dateString.nl}</legend>
                          <label className="k-form-field">
                            <span><i className="k-icon k-i-clock"></i></span>
                            <span className="text-left">{event.dateString.nl}
                              <span className="k-field-info">{event.blockhour.tag}</span></span>
                          </label>
                          <label className="k-form-field">
                            <span><i className="k-icon k-i-myspace"></i></span>
                            <span className="text-left">{event.attendees.length} FysioSporters<span
                              className="k-field-info">{Math.max(0, Math.min(8 - event.attendees.length, 8))} beschikbaar</span></span>
                          </label>
                        </fieldset>

                        <fieldset>
                          <legend><i className="k-icon k-i-myspace"></i>&nbsp; Deelnemers</legend>
                          <label className="k-form-field">
                            <span>Namen</span>
                            <UserMultiSelect ref={this.multiSelectRef}
                                             value={event.attendees.map((x: any) => x.id)}/>
                          </label>
                        </fieldset>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </Dialog>
        </div>
      )
    }

    positiveButtonActionHandler(dialogEvent: DialogEvent) {
      const attendeeIds = this.multiSelectRef.current.widgetInstance.value();
      const eventId: string = this.props.event.id;
      this.props.save.call(this, {eventId, attendeeIds});
    }
  }

  return React.forwardRef((props: any, ref: any) => {
    return <EventDialog {...props} forwardRef={ref}/>;
  });
}

const EventDialog = Component();

export {EventDialog};

