import * as React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

class AlertDialog extends React.Component<{
  positiveAction?: any, negativeAction?: any, open: boolean,
  message: string, positiveButtonText: string, negativeButtonText: string,
  title: string
}> {

  render() {
    const {
      positiveAction, negativeAction, open, message,
      positiveButtonText, negativeButtonText, title
    } = this.props;
    return (
      <div>
        <Dialog
          open={open}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-message"
        >
          <DialogTitle id="alert-dialog-title">{title}</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-message">
              {message}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={negativeAction} color="secondary">
              {negativeButtonText}
            </Button>
            <Button onClick={positiveAction} color="primary" autoFocus>
              {positiveButtonText}
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default AlertDialog;
