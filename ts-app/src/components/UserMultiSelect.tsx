import * as React from 'react';
import {MultiSelect} from "@progress/kendo-dropdowns-react-wrapper";
import {userDataSource} from "../dataSources";
import {inject} from "mobx-react";


@inject('store')
class CustomUserMultiSelect extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
  }

  render() {
    const {isSuperAdmin} = this.props.store.appStore;
    const {forwardRef, value} = this.props;
    return (
      <MultiSelect {...this.props}
                   ref={forwardRef}
                   dataSource={userDataSource}
                   value={value}
                   dataTextField={'text'}
                   dataValueField={'value'}
                   enable={isSuperAdmin()}
      />
    )
  }
}


export const UserMultiSelect = React.forwardRef((props: any, ref: any) => {
  return (
    <CustomUserMultiSelect {...props} forwardRef={ref}/>
  )
});


