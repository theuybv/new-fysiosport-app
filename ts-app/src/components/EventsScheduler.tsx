import * as React from "react";
import '@progress/kendo-ui';
import {Scheduler} from "@progress/kendo-scheduler-react-wrapper";
import {inject, observer} from "mobx-react";

interface IEventSchedule {
  onSchedulerChangeEvent: any;
  events: any;
  store?: any;
}

@inject('store')
@observer
export class EventsScheduler extends React.Component<IEventSchedule, any> {
  views: any[] = [
    {type: "week", selected: true, showWorkHours: false},
  ];
  eventTemplateString = `
      <div class="
          #:Math.max(0, Math.min(8 - attendees.length, 8)) <= 0 ? 'event-not-available' : 'event-available'#
      ">
          <p>
          <span>#:blockhour.tag#</span>
          <br>&nbsp;FysioSportles<br>
          <span>&nbsp;<i class="k-icon k-i-myspace"></i> #: attendees.length #</span>
          <span>beschikbaar: #:Math.max(0, Math.min(8 - attendees.length, 8))#</span>
          </p>
      </div>
    `;
  today: Date = new Date();
  startTime = new Date(this.today.getFullYear()
    + '/' + this.today.getMonth()
    + '/' + this.today.getDate()
    + ' 8:45:00:00'
  );

  constructor(props: any) {
    super(props);
  }



  render() {
    const {onSchedulerChangeEvent, events, store} = this.props;
    const {isSuperAdmin} = store.appStore;
    return (
      <div className={(isSuperAdmin()) ? "is-super-admin": "is-user-admin"}>
        <Scheduler
          dataSource={events}
          views={this.views}
          eventTemplate={this.eventTemplateString}
          startTime={this.startTime}
          change={onSchedulerChangeEvent}
          selectable={true}
          editable={false}
          majorTick={60}
        />

      </div>
    )
  }

  componentDidMount() {

  }
}
