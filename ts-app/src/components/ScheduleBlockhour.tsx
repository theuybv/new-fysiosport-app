import * as React from "react";
import { Checkbox, FormControlLabel } from "@material-ui/core";


interface IProps {
  blockhour: { id: string, startTime: string, dateTime: string, tag: string },
  onChange: any,
  selectedBlockhourIds: any
}

export class ScheduleBlockhour extends React.Component<IProps, any> {
  constructor(props: IProps) {
    super(props);
  }

  render() {
    const { blockhour, selectedBlockhourIds } = this.props;
    return (
      <div>
        <FormControlLabel
          control={
            <Checkbox
          checked={selectedBlockhourIds.includes(blockhour.id)}
          onChange={this._onChange.bind(this)}
          value={blockhour.id}
        />
          }
          label={blockhour.tag}
        />
      </div>
    )
  }

  private _onChange(checkboxEvent: { target: { value: string, checked: boolean } }) {
    this.props.onChange.call(this.props.onChange, checkboxEvent);
  }
}
