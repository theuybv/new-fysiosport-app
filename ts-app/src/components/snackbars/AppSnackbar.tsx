import {Snackbar, SnackbarContent} from "@material-ui/core";
import * as React from "react";
import {inject, observer} from "mobx-react";
import {green, red} from "@material-ui/core/colors";


const styles: any = {
  success: {
    backgroundColor: green[600],
  },
  error: {
    backgroundColor: red[600],
  }
};

interface IState {
  open?: boolean;
  message: string;
  variant?: string;
  action?: any
}

@inject("store")
@observer
export class AppSnackbar extends React.Component<any, any> {

  constructor(props: any) {
    super(props);
    this.state = {
      open: false,
      message: "",
      variant: "success",
      action: undefined
    };
  }

  componentDidMount() {
    this.props.store.appStore.appSnackbar = this;
  }

  render() {
    return (
      <Snackbar
        open={this.state.open}
        {...this.props}
        autoHideDuration={3000}
        anchorOrigin={{vertical: 'bottom', horizontal: 'center'}}
        onClose={() => {
          this.setState({open: false})
        }}
      >
        <SnackbarContent
          style={styles[this.state.variant]}
          aria-describedby="global-app-snackbar"
          message={<span id="global-app-snackbar">{this.state.message}</span>}
          action={this.state.action}
        />
      </Snackbar>
    )
  }

  open(options: IState) {
    this.setState({...options, open: true});
  }

  close() {
    this.setState({open: false});
  }
}
