import * as React from 'react';
import {inject, observer} from "mobx-react";
import {Redirect, Route} from "react-router";

interface IState {
  isVerifying: boolean
}
@inject('store')
@observer
export class AuthenticatedRoute extends React.Component<any, IState> {

  constructor(props: any) {
    super(props);
    this.state = {
      isVerifying: true
    };
  }

  async componentDidMount() {
    await this.props.store.appStore.verifyToken();
    this.setState({isVerifying: false});
  }

  render() {
    return (
      <Route render={() => {
        return (
          (this.props.store.appStore.isAuthenticated)
            ? <this.props.component/>
            : (this.state.isVerifying)
            ? null
            : <Redirect to="/login"/>
        )
      }}/>
    )
  }

}
