import * as React from "react";
import {observer} from "mobx-react";
import {ScheduleBlockhour} from "./ScheduleBlockhour";
import {withStyles} from '@material-ui/core/styles';
import {Typography} from "@material-ui/core";

const styles = {
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    marginBottom: 16,
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
};


interface IProps {
  schedule: {
    dayname: { en: string, nl: string }
    blockhours: any[],
  }
  onSelect: any,
  classes: any,
  selectedShedule: any
}

@observer
class ScheduleDay extends React.Component<IProps, any> {

  constructor(props: IProps) {
    super(props);
  }

  render() {
    const {schedule, onSelect, selectedShedule} = this.props;
    return (
      <div className="card-body">
        <Typography variant="subheading" style={{fontWeight: 'bold'}} className="text-capitalize">{schedule.dayname.nl}</Typography>
        {schedule.blockhours.map((blockhour: any, index: number) => {
          return <ScheduleBlockhour blockhour={blockhour} key={blockhour.id} onChange={onSelect}
                                    selectedBlockhourIds={selectedShedule.blockhourIds}
          />
        })}
      </div>
    )
  }
}

export default withStyles(styles)(ScheduleDay);
