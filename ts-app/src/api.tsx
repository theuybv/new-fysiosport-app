import axios from 'axios';
import {FYSIOSPORT_TOKEN} from "./constants";

const api = axios.create({
  baseURL: `/api`,
  // headers: {'Authorization': 'Bearer ' + localStorage.getItem('token')}
});

// Add a request interceptor
api.interceptors.request.use(function (config) {

  const configCommonHeaders = (localStorage.getItem(FYSIOSPORT_TOKEN))
    ? {...config.headers.common, 'Authorization': 'Bearer ' + localStorage.getItem(FYSIOSPORT_TOKEN)}
    : {...config.headers.common};

  return {
    ...config,
    headers: {
      common: configCommonHeaders
    }
  };
}, function (error) {
  // Do something with request error
  return Promise.reject(error);
});


export default api;
