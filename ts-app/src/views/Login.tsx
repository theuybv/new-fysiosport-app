import * as React from 'react';
import {Button, TextField, Typography, Theme, withStyles} from "@material-ui/core";
import api from "../api";
import {AxiosResponse} from "axios";
import {FYSIOSPORT_EXPIREDAT, FYSIOSPORT_TOKEN, FYSIOSPORT_USER} from "../constants";
import {Redirect, withRouter} from "react-router";
import {inject, observer} from "mobx-react";

const styles = (theme: Theme): any => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  menu: {
    width: 200,
  },
});

@inject('store')
@observer
class Login extends React.Component<any, any> {

  constructor(props: any) {
    super(props);
    this.state = {
      email: "",
      password: "",
      loginErrorMessage: null,
      isVerifying: true
    }
  }

  async componentDidMount() {
    await this.props.store.appStore.verifyToken();
    this.setState({isVerifying: false});
  }

  handleChange = (name: any) => (event: any) => {
    this.setState({
      [name]: event.target.value,
    });
  };

  async authorize() {
    const {email, password} = this.state;

    try {
      const response: AxiosResponse = await api.post(`authorize`, {email, password});
      if (response.data.token && response.data.expiredAt) {
        localStorage.setItem(FYSIOSPORT_TOKEN, response.data.token);
        localStorage.setItem(FYSIOSPORT_EXPIREDAT, response.data.expiredAt);
        localStorage.setItem(FYSIOSPORT_USER, JSON.stringify(response.data.user));
        this.props.history.push('/');
      }
    } catch (e) {
      this.setState({loginErrorMessage: "Uw e-mail of wachtwoord is onjuist"})
    }

  }

  render() {
    const {classes} = this.props;

    return (
      (this.state.isVerifying)
        ? null
        : (this.props.store.appStore.isAuthenticated)
        ? <Redirect to="/dashboard"/>
        : (
          <div className="container">
            <div className="row justify-content-center align-items-center" style={{height: '100vh'}}>
              <div className="col-sm-auto col-12">
                <Typography variant="display1" className="mb-2 text-uppercase text-center">FysioSport
                  Zwanenzijde</Typography>
                <div className="card">
                  <div className="card-body">
                    <form className={classes.container + " d-flex flex-column"} noValidate autoComplete="off">
                      <TextField
                        onChange={this.handleChange('email').bind(this)}
                        id="email"
                        label="Email"
                        placeholder="Uw email adres"
                        className={classes.textField}
                        margin="normal"
                      />
                      <TextField
                        onChange={this.handleChange('password').bind(this)}
                        id="password"
                        type="password"
                        label="Wachtwoord"
                        placeholder="Uw wachtwoord"
                        className={classes.textField}
                        margin="normal"
                      />

                      <div className="mt-4">
                        {(this.state.loginErrorMessage) ? <Typography className="animated fadeIn" variant="subheading"
                                                                      color="error">{this.state.loginErrorMessage}</Typography> : null}
                        <Button disabled={(!this.state.email || !this.state.password)}
                                onClick={this.authorize.bind(this)}
                                className="mt-4 w-100" variant="raised"
                                color="primary">Inloggen</Button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        )
    );
  }
}

const _withRouter = withRouter(Login) as any;
export default withStyles(styles)(_withRouter);
