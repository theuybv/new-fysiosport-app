import * as  React from 'react';
import {Theme, withStyles} from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Hidden from '@material-ui/core/Hidden';
import Divider from '@material-ui/core/Divider';
import MenuIcon from '@material-ui/icons/Menu';
import {Icon, List, ListItem, ListItemIcon, ListItemText} from "@material-ui/core";
import {Link, Redirect, Route, Switch, withRouter} from "react-router-dom";
import '../app.css';
import {inject, observer} from "mobx-react";
import {FYSIOSPSORT_ROLE_SUPER_ADMIN} from "../constants";
import EventsCalendar from "./EventsCalendar";
import AnnualSchedule from "./AnnualSchedule";

const drawerWidth = 240;

const styles = (theme: Theme): any => ({
  root: {
    flexGrow: 1,
    height: '100vh',
    zIndex: 1,
    position: 'relative',
    display: 'flex',
    width: '100%',
  },
  appBar: {
    position: 'fixed',
    marginLeft: drawerWidth,
    [theme.breakpoints.up('md')]: {
      width: `calc(100% - ${drawerWidth}px)`,
    },
  },
  navIconHide: {
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
  toolbar: theme.mixins.toolbar,
  drawerPaper: {
    width: drawerWidth,
    [theme.breakpoints.up('md')]: {
      position: 'fixed',
    },
  },
  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing.unit * 3,
    [theme.breakpoints.up('md')]: {
      paddingLeft: 255
    }
  },
});

@inject('store')
@observer
class Dashboard extends React.Component<any, any> {
  state = {
    mobileOpen: false,
  };

  handleDrawerToggle = () => {
    this.setState({mobileOpen: !this.state.mobileOpen});
  };

  componentDidMount() {
    const {store} = this.props;
    store.appStore.fetchAllSchedules();
    store.appStore.fetchAllEvents();
  }

  render() {
    const {classes, theme} = this.props;

    debugger

    const drawer = (
      <div>
        <div className={classes.toolbar}>
          <div className="d-flex flex-row justify-content-start">
            <img src="http://ondemand.causeffect.nl/wp-content/uploads/2017/07/unnamed.png" alt="" height="64"
                 className="ml-3"/>
          </div>
        </div>
        <Divider/>
        <List component="nav">
          <ListItem button component={(props: any) => <Link to="/dashboard/calendar" {...props}/>}>
            <ListItemIcon>
              <Icon>date_range</Icon>
            </ListItemIcon>
            <ListItemText primary="Lessen kalender"/>
          </ListItem>
          {
            (this.props.store.appStore.authenticatedUser.role === FYSIOSPSORT_ROLE_SUPER_ADMIN)
              ?
              (
                <ListItem button component={(props: any) => <Link to="/dashboard/scheduler" {...props}/>}>
                  <ListItemIcon>
                    <Icon>chrome_reader_mode</Icon>
                  </ListItemIcon>
                  <ListItemText primary="Jaarplanning"/>
                </ListItem>
              )
              : null
          }
        </List>
      </div>
    );

    return (
      <div className={classes.root}>
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton
              color="inherit"
              aria-label="open drawer"
              onClick={this.handleDrawerToggle}
              className={classes.navIconHide}
            >
              <MenuIcon/>
            </IconButton>
            <Typography variant="title" color="inherit" noWrap>
              FysioSport Inplan Systeem
            </Typography>
          </Toolbar>
        </AppBar>
        <Hidden mdUp>
          <Drawer
            variant="temporary"
            anchor={theme.direction === 'rtl' ? 'right' : 'left'}
            open={this.state.mobileOpen}
            onClose={this.handleDrawerToggle}
            classes={{
              paper: classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}
          >
            {drawer}
          </Drawer>
        </Hidden>
        <Hidden smDown implementation="css">
          <Drawer
            variant="permanent"
            open
            classes={{
              paper: classes.drawerPaper,
            }}
          >
            {drawer}
          </Drawer>
        </Hidden>
        <main className={classes.content}>
          <div className={classes.toolbar}/>
          <div className="container-fluid">
            <Switch>
              <Route path={`/dashboard/calendar`} component={EventsCalendar}/>
              <Route path={`/dashboard/scheduler`} component={AnnualSchedule}/>
              <Route render={() => <Redirect to={"/dashboard/calendar"}/>}/>
            </Switch>
          </div>
        </main>
      </div>
    );
  }
}


export default withStyles(styles, {withTheme: true})(withRouter(Dashboard) as any);
