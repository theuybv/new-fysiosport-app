import * as React from 'react';
import {EventsScheduler} from "../components/EventsScheduler";
import {inject, observer} from "mobx-react";
import {EventDialog} from "../components/dialogs/EventDialog";
import api from "../api";
import SchedulerChangeEvent = kendo.ui.SchedulerChangeEvent;
import Dialog = kendo.ui.Dialog;

interface IApp {
  store?: any;
}

@inject('store')
@observer
class EventsCalendar extends React.Component<IApp, any> {
  dialogRef: any = React.createRef<Dialog>();


  constructor(props: any) {
    super(props);
  }

  render() {
    const {selectedEvent, events} = this.props.store.appStore;

    return (
      <div className="row">
        <div className="col-12">
          <EventDialog event={selectedEvent}
                       ref={this.dialogRef}
                       save={this.replaceEventAttendees.bind(this)}/>
          <EventsScheduler
            onSchedulerChangeEvent={this.onSchedulerChangeEvent.bind(this)}
            events={events.toJSON()}
          />
        </div>
      </div>
    );
  }

  componentDidMount() {
  }

  private _openEventDialog(event: any) {
    this.props.store.appStore.selectEvent(event);
    this.dialogRef.current.widgetInstance.open();
    this.dialogRef.current.widgetInstance.title(`${event.dateString.nl} | ${event.blockhour.tag}`)
  }

  onSchedulerChangeEvent(e: SchedulerChangeEvent) {
    const {isSuperAdmin} = this.props.store.appStore;
    const event: any = e.events[0];
    const startDate = e.start as Date;
    const endDate = e.end as Date;

    if (event && (startDate.getHours() !== endDate.getHours())) {
      const isAvailable: boolean = (Math.max(0, Math.min(8 - event.attendees.length, 8)) > 0);
      if (isSuperAdmin()) {
        this._openEventDialog(event);
      } else {
        if (isAvailable) {
          //@todo implement reservation dialog
          alert("reservation dialog")
        }
      }
    }
  }

  async replaceEventAttendees({eventId, attendeeIds}: any) {
    const {appStore} = this.props.store;
    try {
      await api.put(`event/${eventId}/attendee`, {
        attendeeIds: attendeeIds
      });
      appStore.fetchAllEvents();
      this.dialogRef.current.widgetInstance.close();
      appStore.appSnackbar.open({message: `De FysioSport les is opgeslagen`, variant: 'success'});
    } catch (e) {
      console.log(e);
      appStore.appSnackbar.open({message: e.toString(), variant: 'error'});
    }
  }
}

export default EventsCalendar;
