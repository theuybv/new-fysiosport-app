import * as React from 'react';
import {UserMultiSelect} from "../components/UserMultiSelect";
import {inject, observer} from "mobx-react";
import ScheduleDay from "../components/ScheduleDay";
import {Button, Icon, Theme, Typography, withStyles} from "@material-ui/core";
import {Link} from "react-router-dom";
import {CancelAnnualBlockhoursDialog} from "../components/dialogs/CancelAnnualBlockhoursDialog";
import api from "../api";


interface IState {
  attendeeIds: string[],
  schedules: { dayname: string, blockhourIds: string[] }[],
  cancelDialog?: {
    open: boolean,
    message?: string,
  }
}

const styles = (theme: Theme): any => ({
  autocomplete: {
    [theme.breakpoints.up('sm')]: {
      position: 'fixed'
    }
  },
  button: {
    margin: theme.spacing.unit,
  }
});

const initState = {
  attendeeIds: [],
  schedules: [
    {dayname: "monday", blockhourIds: []},
    {dayname: "tuesday", blockhourIds: []},
    {dayname: "wednesday", blockhourIds: []},
    {dayname: "thursday", blockhourIds: []},
    {dayname: "friday", blockhourIds: []},
    {dayname: "saturday", blockhourIds: []},
  ],
  cancelDialog: {
    open: false,
    message: ``
  }
};


@inject('store')
@observer
class AnnualSchedule extends React.Component<any, IState> {

  state = JSON.parse(JSON.stringify(initState));
  userMultiSelectRef: any = React.createRef();
  snackBarActionButton = () => {
    return (
      <Button variant="outlined" style={{color: 'white'}}
              component={(props: any) => <Link to="/dashboard/calendar" {...props} />}>Naar
        kalender</Button>
    )
  };

  constructor(props: any) {
    super(props);
  }

  componentDidMount() {
    (this.userMultiSelectRef.current.widgetInstance)
      ? this.userMultiSelectRef.current.widgetInstance.focus()
      : null;
  }

  render() {
    const {store, classes} = this.props;
    return (
      <div>

        <CancelAnnualBlockhoursDialog
          open={this.state.cancelDialog.open}
          positiveAction={this.cancelAnnualSchedule.bind(this)}
          negativeAction={() => this.setState({cancelDialog: {open: false}})}/>

        <div className="row">
          <div className="col-md-3 col-lg-3 col-sm-12 col-xs-12 mb-4">
            <div className="row">
              <div className={`col-md-12 col-sm-12 col-xs-12 col-lg-12`}>
                <div className={``}>
                  <Typography variant="headline" className="mb-2">Voor heel jaar {new Date().getFullYear()}</Typography>
                  <Typography variant="subheading">Selecteer FysioSporters</Typography>
                  <UserMultiSelect ref={this.userMultiSelectRef} value={this.state.attendeeIds} change={(e: any) => {
                    this.setState({attendeeIds: e.sender.value()})
                  }}/>
                  {(this.formIsValid()) ?
                    <div className="mt-4 animated fadeIn">

                      <Button variant="raised" className={classes.button + ' w-100'} color="primary"
                              onClick={this.doAnnualSchedule.bind(this)}><Icon>save</Icon>&nbsp; Plan
                        FysioSporters in</Button>

                      <hr/>
                      <Button className={classes.button + ' w-100'} color="secondary"
                              onClick={() => this.setState({cancelDialog: {open: true}})}>
                        <Icon>clear</Icon>Verwijder uit de planning
                      </Button>
                    </div> : null}
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-9 col-lg-9 col-sm-12 col-xs-12">
            <Typography variant="headline" className="mb-2">Selecteer de tijden</Typography>
            <div className="d-flex flex-wrap">
              {store.appStore.schedules.map((schedule: any, index: number) => {
                return (
                  <div key={schedule.dayname.en}
                       className="col-12 col-lg-3 col-xl-3 col-md-12 col-sm-12 col-xs-12 card mr-4 mb-4">
                    <ScheduleDay schedule={schedule}
                                 selectedShedule={this.state.schedules.find((item: any) => item.dayname === schedule.dayname.en)}
                                 onSelect={(e: any) => {
                                   this.onSelect.apply(this, [schedule.dayname.en, e]);
                                 }}/>
                  </div>
                )
              })}
            </div>
          </div>
        </div>
      </div>
    )
  }

  formIsValid() {
    const blockhourIds: any = this.state.schedules.map((schedule: any) => schedule.blockhourIds);
    const totalSelectedBlockhour = blockhourIds.map((item: any) => item.length)
      .reduce((acc: number, cur: number) => acc + cur);
    return (totalSelectedBlockhour > 0 && this.state.attendeeIds.length);
  }

  onSelect(dayname: string, checkboxEvent: { target: { value: string, checked: boolean } }) {
    const stateSchedule: any = this.state.schedules.find((_stateSchedule: any) => {
      return _stateSchedule.dayname === dayname;
    });

    if (checkboxEvent.target.checked) {
      stateSchedule.blockhourIds = [...stateSchedule.blockhourIds, checkboxEvent.target.value];
    } else {
      stateSchedule.blockhourIds.splice(
        stateSchedule.blockhourIds.indexOf(checkboxEvent.target.value),
        1
      )
    }
    this.setState({...stateSchedule});
  }

  async cancelAnnualSchedule() {
    const formData: IState = {
      attendeeIds: this.state.attendeeIds,
      schedules: this.state.schedules.filter((schedule: any) => schedule.blockhourIds.length)
    };

    try {
      await api.delete(`/schedule/annual`, {
        data: JSON.stringify(formData)
      });
      this.props.store.appStore.fetchAllEvents();
      this.setState({
        ...JSON.parse(JSON.stringify(initState)),
        schedules: JSON.parse(JSON.stringify(initState)).schedules,
      });
      this.props.store.appStore.appSnackbar.open({
        message: `De FysioSporters zijn uit de planning verwijderd`,
        action: this.snackBarActionButton(),
      });
    } catch (e) {
      this.props.store.appStore.appSnackbar.open({
        message: e.toString(),
        variant: 'error'
      });
    }
  }

  async doAnnualSchedule() {
    const formData: IState = {
      attendeeIds: this.state.attendeeIds,
      schedules: this.state.schedules.filter((schedule: any) => schedule.blockhourIds.length)
    };

    try {
      await api.post(`/schedule/annual`, formData);
      this.props.store.appStore.fetchAllEvents();
      this.setState({
        ...JSON.parse(JSON.stringify(initState)),
        schedules: JSON.parse(JSON.stringify(initState)).schedules,
      });
      this.props.store.appStore.appSnackbar.open({
        message: `De FysioSporters ingepland`,
        action: this.snackBarActionButton(),
      });
    } catch (e) {
      this.props.store.appStore.appSnackbar.open({
        message: e.toString(),
        variant: 'error'
      });
    }
  }
}

export default withStyles(styles, {withTheme: true})(AnnualSchedule);
