/**
 * DataSourceController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  user: async (req, res) => {
    const users = await User.find({firstName: {'!=': 'NULL' }});
    res.json(
      JSON.parse(JSON.stringify(users))
        .map(user => {
          const infix = (user.infix) ? `${user.infix} ` : ' ';
          return {
            text: `${user.fullName}`,
            value: user.id,
            //  color: `${'#'+Math.floor(Math.random()*16777215).toString(16)}`
          }
        })
    );
  }
};

