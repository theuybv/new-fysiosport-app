const passport = require('passport');
const jwt = require('jsonwebtoken');

module.exports = function (req, res) {
  jwt.verify(req.body.token, sails.config.passport.secretKey, (err, decoded) => {
    if (err) {
      return res.serverError(err);
    }
    return res.json(_.omit(decoded, ['password', 'email']));
  });
};
