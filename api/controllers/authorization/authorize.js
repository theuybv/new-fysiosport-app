const passport = require('passport');
const jwt = require('jsonwebtoken');
const moment = require('moment');

module.exports = function (req, res) {
  passport.authenticate('local', {session: false}, (err, user, info) => {
    if (err || !user) {
      return res.status(400).json({
        message: info ? info.message : 'Login failed',
        user: user
      });
    }
    const token = jwt.sign(user, sails.config.passport.secretKey, {expiresIn: '3h'});
    return res.json({
      user: user,
      token: token,
      expiredAt: moment().add(3, 'hours')._d.getTime()
    });
  })
  (req, res);
};
