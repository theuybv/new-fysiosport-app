module.exports = async (req, res) => {
  try {
    const events = await sails.helpers.eventsCanceler.with({
      schedules: req.body.schedules,
      attendeeIds: req.body.attendeeIds
    });
    res.json(events);
  } catch (e) {
    res.serverError(e);
  }
};
