module.exports = async (req, res) => {
  const schedule = await Schedule.find().populate('blockhours');
  res.json(schedule);
};
