/**
 * UserController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  index: async (req, res) => {
    const users = await User.find().omit(["email"]);
    res.json(users);
  },
  addEvents: async (req, res) => {
    const userId = req.data.userId;
    const eventIds = req.data.eventIds;
    await User.addToCollection(userId, 'events', eventIds);
  },
  removeEvents: async (req, res) => {
    const userId = req.data.userId;
    const eventIds = req.data.eventIds;
    await User.removeFromCollection(userId, 'events', eventIds);
  }
};

