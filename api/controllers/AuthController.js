/**
 * AuthController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const asAction = require('machine-as-action');


module.exports = {
  secret: asAction({
    inputs: {
    },
    exits: {
      success: {
        outputExample: 'Some dynamic message like this.'
      }
    },
    fn: function (inputs, exits) {
      // sails.log(this.req.body);
      return exits.success('Hello world!');
    }
  })
};

