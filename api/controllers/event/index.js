module.exports = {


  friendlyName: 'Event index',


  description: '',


  inputs: {

  },


  exits: {

    myCustomError: {
      description: 'Some error found'
    }

  },


  fn: async function (inputs, exits) {

    // sails.log(this.req);
    // sails.log(this.res);


    const events = await Event.getLatestEvents();
    // All done.
    return exits.success(events);

  }


};



