module.exports = {
  friendlyName: "",
  description: "",
  inputs: {
    attendeeIds: {
      type: "ref",
      custom: function (value) {
        return _.isArray(value);
      }
    }
  },
  exits: {
    wrongEventIdType: {
      description: "Wrong event Id Type"
    }
  },
  fn: async function (inputs, exits) {
    const req = this.req;
    const res = this.res;

    if (!_.parseInt(req.params.id)) {
      throw "wrongEventIdType";
    }
    const eventId = req.params.id;
    const attendeeIds = inputs.attendeeIds;

    try {
      const events = await sails.helpers.replaceAttendees.with({
        eventId, attendeeIds
      });
      return res.json(events);
    } catch (e) {
      return res.serverError(e);
    }
  }
};
