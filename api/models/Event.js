const moment = require('moment');
/**
 * Event.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    dayname: {
      type: 'string'
    },
    date: {
      type: 'string',
      required: true
    },
    startDatetime: {type: 'string', required: true},
    endDatetime: {type: 'string', required: true},
    blockhour: {
      model: 'blockhour',
    },
    attendees: {
      collection: 'user',
      via: 'events'
    }
    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝


    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝
  },
  customToJSON: function () {
    // Return a shallow copy of this record with the password and ssn removed.
    return {
      ...this,
      dayname: {
        en: moment(this.date).format('dddd').toLowerCase(),
        nl: moment(this.date).locale('nl').format('dddd').toLowerCase()
      },
      dateString: {
        en: moment(this.date).format('dddd D MMMM YYYY'),
        nl: moment(this.date).locale('nl').format('dddd D MMMM YYYY')
      }
    };
  },
  getLatestEvents: async () => {
    const blockedDates = await BlockedDate.find();

    const events = await Event
      .find({
        date: {nin: blockedDates.map(blockedDate => blockedDate.date)}
      })
      .populate('blockhour')
      .populate('attendees', {
        sort: 'lastName ASC'
      });
    const mappedEvents = JSON.parse(JSON.stringify(events)).map(event => {
      return {
        ...event,
        start: event.startDatetime,
        end: event.endDatetime,
        title: event.dateString.nl,
        // attendees: event.attendees.map(attendee => attendee.id)
      };
    });

    return _.flatten([...mappedEvents, blockedDates.map(blockedDate => {
      return {
        start: moment(blockedDate.date)._d,
        end: moment(blockedDate.date).add(24, 'hours')._d,
        title: "Geen FysioSport les!"
      }
    })]);
  }
};

