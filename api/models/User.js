/**
 * User.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  schema: true,
  attributes: {
    'role': {
      type: 'string',
      defaultsTo: 'user_admin'
    },
    'password': {
      type: 'string',
      encrypt: true,
      required: true
    },
    'email': {
      type: 'string',
      required: true
    },
    'lastName': {
      type: 'string',
      required: true
    },
    'infix': {
      type: 'string',
    },
    'firstName': {
      type: 'string',
      required: true
    },
    'events': {
      collection: 'event',
      via: 'attendees'
    },
    'blockhours': {
      collection: 'blockhour',
      via: 'attendees'
    }
    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝


    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝
    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

  },
  customToJSON: function () {
    // Return a shallow copy of this record with the password and ssn removed.
    const infix = (this.infix) ? this.infix + ' ' : ' ';
    const fullName = `${this.firstName}${infix}${this.lastName}`;
    return _.omit(_.extend(this, {fullName: fullName}), ['password', 'updatedAt', 'createdAt', 'email']);
  }

};

