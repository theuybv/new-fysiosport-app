const Moment = require('moment');
const MomentRange = require('moment-range');
const moment = MomentRange.extendMoment(Moment);
const _ = require('lodash');

module.exports = {


  friendlyName: 'Events scheduler',


  description: '',


  inputs: {
    schedules: {
      type: 'json',
      custom: (value) => {
        return _.isArray(value) && value.length > 0;
      }
    },
    attendeeIds: {
      type: 'json',
      custom: (value) => {
        return _.isArray(value) && value.length > 0;
      }
    }
  },


  exits: {},


  fn: async function (inputs, exits) {

    const selectedBlockhourIds = _.flatten(inputs.schedules.map(schedule => schedule.blockhourIds));
    const selectedDaynames = inputs.schedules.map(schedule => schedule.dayname);
    const eventsWithBlockhoursAndUsers = await Event.find({
      blockhour: {'in': selectedBlockhourIds},
      dayname: {in: selectedDaynames},
    })
      .sort('date ASC')
      .populate('attendees');

    for (event of eventsWithBlockhoursAndUsers) {
      if(event.attendees.length) {
        for (attendee of event.attendees) {
          const diffAttendeeIds = _.difference(
            inputs.attendeeIds.map(attendeeId => attendeeId.toString()),
            event.attendees.map(attendee => attendee.id)
          );
          await Event.addToCollection(event.id, 'attendees', diffAttendeeIds);
        }
      } else {
        await Event.addToCollection(event.id, 'attendees', inputs.attendeeIds);
      }
    }

    const events = await Event.find().populate('blockhour').populate('attendees');

    // All done.
    return exits.success(events);

  }


};

