process.env.TZ = 'Europe/Amsterdam';

const MongoClient = require('mongodb').MongoClient;
const Moment = require('moment');
const MomentRange = require('moment-range');
const moment = MomentRange.extendMoment(Moment);
const _ = require('lodash');
require('dotenv').config();


// Connection URL
const url = process.env.MONGO_MIGRATION_CONNECTION_URL;
// Database Name
const dbName = process.env.MONGO_MIGRATION_DB_NAME;

module.exports = {


  friendlyName: 'Do migration',


  description: '',


  inputs: {},


  exits: {},


  fn: async function (inputs, exits) {


    const client = await MongoClient.connect(url);
    const db = client.db(dbName);
    const allBlockhoursFromMongoLab = await db.collection('blockhours').find({}).toArray();
    const allUsersFromMongoLab = await db.collection('users').find({}).toArray();
    const allBlockedDatesFromMongoLab = await db.collection('blocked_dates').find({}).toArray();

    const getBlockhourById = (blockhours, id) => blockhours.find(blockhour => blockhour._id === id);
    const getUsersByIds = (users, ids) => users.filter((user) => _.includes(ids, user._id));

    const mappedBlockhours = allBlockhoursFromMongoLab.map((blockhour) => {
      const startTime = moment(blockhour.from_time).format('HH:mm');
      const endTime = moment(blockhour.to_time).format('HH:mm');
      return {
        id: blockhour._id.toString(),
        tag: `${startTime}-${endTime}`,
        startTime: startTime,
        endTime: endTime
      };
    });
    /**=== MIGRATE blockhours */
    const createdBlockhours = await Blockhour.createEach(mappedBlockhours).fetch();
    /**=== MIGRATE weekdays ==> schedule */
    await Schedule.createEach([
      {
        id: '1',
        dayname: 'monday',
        blockhours: createdBlockhours.filter(blockhour => _.includes([
          '09:00-10:00',
          '10:00-11:00',
          '19:30-20:30',
          '20:45-21:45'
        ], blockhour.tag)).map(blockhour => blockhour.id)
      },
      {
        id: '2',
        dayname: 'tuesday',
        blockhours: createdBlockhours.filter(blockhour => _.includes([
          '09:00-10:00'
        ], blockhour.tag)).map(blockhour => blockhour.id)
      },
      {
        id: '3',
        dayname: 'wednesday',
        blockhours: createdBlockhours.filter(blockhour => _.includes([
          '09:00-10:00',
          '10:15-11:15',
          '18:00-19:00',
          '19:15-20:15'
        ], blockhour.tag)).map(blockhour => blockhour.id)
      },
      {
        id: '4',
        dayname: 'thursday',
        blockhours: createdBlockhours.filter(blockhour => _.includes([
          '09:00-10:00',
          '13:30-14:30',
          '19:30-20:30',
          '20:45-21:45',
        ], blockhour.tag)).map(blockhour => blockhour.id)
      },
      {
        id: '5',
        dayname: 'friday',
        blockhours: createdBlockhours.filter(blockhour => _.includes([
          '09:00-10:00',
          '14:00-15:00'
        ], blockhour.tag)).map(blockhour => blockhour.id)
      },
      {
        id: '6',
        dayname: 'saturday',
        blockhours: createdBlockhours.filter(blockhour => _.includes([
          '08:45-09:45'
        ], blockhour.tag)).map(blockhour => blockhour.id)
      }
    ]);

    const mappedUsers = allUsersFromMongoLab.map((user) => {
      return {
        ...user,
        id: user._id.toString(),
        infix: user.tussenvoegsel,
        firstName: user.firstname,
        lastName: user.lastname,
      };
    });
    // /**=== MIGRATE users ==> schedule */
    await User.createEach(mappedUsers).fetch();

    const mappedBlockedDates = allBlockedDatesFromMongoLab.map((blockedDate) => {
      return {
        id: blockedDate._id.toString(),
        date: moment(moment(blockedDate.date).format('YYYY-MM-DD'), 'YYYY-MM-DD')._d
      };
    });
    /**=== MIGRATE users ==> blocked_dates */
    await BlockedDate.createEach(mappedBlockedDates).fetch();


    const createMappedEvents = (reservations) => {
      let mappedEvents = [];
      for (reservation of reservations) {
        const blockhoursUsers = reservation.blockhours_users;
        for (blockhourUser of blockhoursUsers) {
          const blockhour = getBlockhourById(allBlockhoursFromMongoLab, blockhourUser.blockhour);
          const userIds = blockhourUser.users;
          const attendees = getUsersByIds(allUsersFromMongoLab, userIds);
          const startDateTime = moment(
            `${moment(reservation.date).format('YYYY-MM-DD')} ${moment(blockhour.from_time).format('HH:mm')}`,
            'YYYY-MM-DD HH:mm'
          )._d.toISOString();
          const endDatetime = moment(
            `${moment(reservation.date).format('YYYY-MM-DD')} ${moment(blockhour.to_time).format('HH:mm')}`,
            'YYYY-MM-DD HH:mm'
          )._d.toISOString();

          mappedEvents.push({
            id: (_.uniqueId()).toString(),
            dayname: moment(reservation.date).format('dddd').toLowerCase(),
            date: moment(moment(reservation.date).format('YYYY-MM-DD'), 'YYYY-MM-DD')._d,
            blockhour: blockhour._id.toString(),
            startDatetime: startDateTime,
            endDatetime: endDatetime,
            attendees: attendees.map(user => user._id.toString())
          });
        }
      }
      return mappedEvents;
    };

    /** Migrate reservations ==> events data where date is gte today */
    const targetDate = moment(moment().format('YYYY-MMM-DD'), 'YYYY-MMM-DD');
    const reservationsFromMongoLab = await db.collection('reservations')
      .find({date: {'$gte': targetDate._d}})
      .sort({date: 1})
      .toArray();
    await Event.createEach(createMappedEvents(reservationsFromMongoLab)).fetch();
    /*================ END MIGRATION ===*/////////////////////

    return exits.success();

  }


};

