const Moment = require('moment');
const MomentRange = require('moment-range');
const moment = MomentRange.extendMoment(Moment);
const _ = require('lodash');

module.exports = {


  friendlyName: 'Events canceler',


  description: '',


  inputs: {
    schedules: {
      type: 'json'
    },
    attendeeIds: {
      type: 'json'
    }
  },


  exits: {},


  fn: async function (inputs, exits) {

    const selectedBlockhourIds = _.flatten(inputs.schedules.map(schedule => schedule.blockhourIds));
    const selectedDaynames = inputs.schedules.map(schedule => schedule.dayname);
    const eventsWithBlockhoursAndUsers = await Event.find({
      blockhour: {'in': selectedBlockhourIds},
      dayname: {in: selectedDaynames},
    })
      .sort('date ASC')
      .populate('attendees', {
        id: {in: inputs.attendeeIds}
      });

    for (event of eventsWithBlockhoursAndUsers) {
      for (attendee of event.attendees) {
        const intersectAttendeeIds = _.intersection(inputs.attendeeIds.map(attendeeId => attendeeId.toString()), event.attendees.map(attendee => attendee.id));
        await Event.removeFromCollection(event.id, 'attendees', intersectAttendeeIds);
      }
    }

    const events = await Event.find().populate('blockhour').populate('attendees');

    // All done.
    return exits.success(events);

  }


};

