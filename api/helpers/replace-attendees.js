module.exports = {


  friendlyName: 'Replace attendees',


  description: '',


  inputs: {
    eventId: {
      type: "string"
    },
    attendeeIds: {
      type: "json",
      custom: (value) => {
        return _.isArray(value);
      }
    }
  },


  exits: {
    reaplaceAttendeeError: {
      description: 'Failed to update attendee list for a specific event'
    }
  },


  fn: async function (inputs, exits) {

    const {eventId, attendeeIds} = inputs;

    try {
      await Event.replaceCollection(eventId, 'attendees')
        .members(attendeeIds);

      return exits.success(
        Event.findOne({id: eventId})
          .populate('attendees')
          .populate('blockhour')
      );
    } catch (e) {
      console.log(e);
      throw 'reaplaceAttendeeError';
    }
  }


};

