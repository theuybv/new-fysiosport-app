module.exports = (req, res, next) => {

  if (req.user.role === 'super_admin') {
    return next();
  } else {
    return res.status(403).json({message: "You don't have permission to access this endpoint"});
  }
};
