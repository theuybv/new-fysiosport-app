require('./lifecycle.test');
const chai = require('chai');
const expect = chai.expect;
const supertest = require('supertest');
const _ = require('lodash');


const ATTENDEES_IDS = {
  THEUY: '8746',
  HUUB: '12128',
  MELANIE: '167',
  ANDRE: '11',
  ATE: '19',
  TJEERD: '124',
  WIM: '128',
  ANTON: '160',
  BERT: '67995',
};
const formData = {
  schedules: [
    {
      dayname: 'saturday',
      blockhourIds: ['2']
    }
  ],
  attendeeIds: [
    ATTENDEES_IDS.THEUY,
    ATTENDEES_IDS.HUUB,
    ATTENDEES_IDS.MELANIE,
    ATTENDEES_IDS.ANDRE,
    ATTENDEES_IDS.ATE,
    ATTENDEES_IDS.TJEERD,
    ATTENDEES_IDS.WIM,
    ATTENDEES_IDS.ANTON,
    ATTENDEES_IDS.BERT
  ]
};


describe('Test Api actions, including migration', async () => {

  const getLatestSaturdayEvents = async () => {
    return Event.find({
      dayname: "saturday"
    })
      .sort('date DESC')
      .populate('attendees')
      .populate('blockhour')
      .limit(1);
  };


  describe('Migration from old app', () => {
    it('should have this year latest event', async () => {
      const events = await Event.find().sort("date DESC").limit(1);
      expect(events.length).to.be.above(0);
      expect(events[0].date).to.have.string("2018-12-30");
    });
  });

  describe('Annual Events scheduler and canceler', () => {

    it('should schedule multiple attendee into specific blockhour', async () => {
      await sails.helpers.eventsScheduler.with({
        attendeeIds: formData.attendeeIds,
        schedules: formData.schedules,
      });

      const events = await getLatestSaturdayEvents();
      expect(events[0].attendees)
        .to.have.lengthOf(formData.attendeeIds.length);

      expect(events[0].attendees.map(attendee => attendee.id))
        .to.include.members(formData.attendeeIds);
    });

    it('should cancel multiple attendee into specific blockhour', async () => {
      await sails.helpers.eventsCanceler.with({
        attendeeIds: formData.attendeeIds,
        schedules: formData.schedules,
      });

      const events = await getLatestSaturdayEvents();
      expect(events[0].attendees)
        .to.have.lengthOf(0);
    });

  });

  describe('AnnualSchedulerController', () => {

    it('should addAttendeesToSchedules', async () => {
      const response = await supertest(sails.hooks.http.app)
        .post('/api/schedule/annual')
        .send(formData)
        .expect('Content-Type', /json/)
        .expect(200);

      const events = await getLatestSaturdayEvents();
      expect(events[0].attendees)
        .to.have.lengthOf(formData.attendeeIds.length);

      expect(events[0].attendees.map(attendee => attendee.id))
        .to.include.members(formData.attendeeIds);

    });

    it('should removeAttendeesToSchedules', async () => {
      const response = await supertest(sails.hooks.http.app)
        .delete('/api/schedule/annual')
        .send(formData)
        .expect('Content-Type', /json/)
        .expect(200);

      const events = await getLatestSaturdayEvents();
      expect(events[0].attendees)
        .to.have.lengthOf(0);


    });

  });

  describe('EventController', () => {

    it('should replaceAttendee for specific event', async () => {
      const events = await Event.find().limit(1).sort("date DESC").populate('attendees');
      expect(events[0].attendees).to.have.length.above(0);
      expect(events[0].id).to.be.a("string");

      const attendee = await User.findOne({id: ATTENDEES_IDS.THEUY});

      const data = {
        attendeeIds: [ATTENDEES_IDS.THEUY]
      };

      const response = await supertest(sails.hooks.http.app)
        .put(`/api/event/${events[0].id}/attendee`)
        .send(data)
        .expect('Content-Type', /json/)
        .expect(200);

      expect(response.body.attendees.map(attendee => attendee.id))
        .to.eql(data.attendeeIds);
      expect(response.body.attendees[0].id).to.equal(attendee.id);

    });


  });

  describe('Authorization Controller', () => {
    const login = (credentials) => {
      return supertest(sails.hooks.http.app)
        .post(`/api/authorize`)
        .send(credentials);
    };
    const fakeAccount = {
      id: '213232323',
      email: 'test@test.nl',
      password: '123',
      firstName: "TestPerson",
      lastName: "TestPerson Lastname"
    };

    before(async () => {
      const user = await User.create(Object.assign({}, fakeAccount)).fetch();
      expect(user.email).to.eql(fakeAccount.email);
      expect(user.password).not.to.equal(fakeAccount.password);
    });

    after(async () => {
      await User.destroy({
        id: fakeAccount.id
      });
    });

    it("should authorize with email and password and return correct json webtoken", async () => {

      const response = await login({
        email: fakeAccount.email,
        password: fakeAccount.password
      })
        .expect(200);

      expect(response.body.token).to.match(/^[A-Za-z0-9-_=]+\.[A-Za-z0-9-_=]+\.?[A-Za-z0-9-_.+/=]*$/)
    });

    it("should deny authorization with invalid credentials", async () => {

      const response = await login({
        email: "",
        password: ""
      })
        .expect(400);
    });

    describe("test protected endpoints by JSON webtoken", () => {
      let token;
      before(async () => {
        const response = await login({
          email: fakeAccount.email,
          password: fakeAccount.password
        }).expect(200);
        token = response.body.token;
      });

      it("should protect /api/secret endpoint due missing bearer token", async () => {
        await supertest(sails.hooks.http.app)
          .get(`/api/secret`)
          .expect(403);
      });

      it("should bypass protected endpoint", async () => {
        const res = await supertest(sails.hooks.http.app)
          .post(`/api/secret`)
          .set('Accept', 'application/json')
          .set('Authorization', "Bearer " + token)
          .expect(200, "Hello world!");
      });

    });

  });

});
