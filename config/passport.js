require('dotenv').config();
const passport = require('passport');

const passportJWT = require("passport-jwt");

const ExtractJWT = passportJWT.ExtractJwt;
const LocalStrategy = require('passport-local').Strategy;
const JWTStrategy = passportJWT.Strategy;

const config = {
  secretKey: process.env.JSONWEBTOKEN_SECRET_KEY
};

// this middleware triggers only the HTTP POST method
passport.use('local', new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password'
  },
  function (email, password, cb) {


    //Assume there is a DB module pproviding a global UserModel
    return User.findOne({email: email}).decrypt()
      .then(user => {
        if (!user) {
          return cb(null, false, {message: 'Incorrect email or password.'});
        } else {
          if (password.toString().toLowerCase() === user.password.toString().toLowerCase()) {
            return cb(null, user, {
              message: 'Logged In Successfully'
            });
          } else {
            return cb(null, false, {message: 'Incorrect email or password.'});
          }
        }
      })
      .catch(err => {
        return cb(err);
      });
  }
));

passport.use(new JWTStrategy({
    jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
    secretOrKey: config.secretKey
  },
  function (jwtPayload, cb) {
    //find the user in db if needed
    return User.findOne({id: jwtPayload.id})
      .then(user => {
        return cb(null, user);
      })
      .catch(err => {
        return cb(err);
      });
  }
));


module.exports.passport = config;
